'use strict';


angular.module('kprfDirectives', [])
    .directive("scroll", function () {
        return function (scope, element, attrs) {
            angular.element(element).bind("scroll", function () {
                scope[attrs.scroll] = true;
                scope.$apply();
            });
        };
    })
    .directive('daModal', function () {
        return {
            restrict: 'A',
            link: function ($scope, $element, $attrs) {
                console.log('modal');

                $element.bind('click', function () {
                    console.log('click');
                    angular.element($attrs.daModal)
                        .modal({
                            //blurring: true,
                            onDeny: function () {
                                console.log('false');
                                return true;
                            },
                            onApprove: function () {
                                console.log($scope, 'true');
                                $scope.delete();
                                return true;
                            }
                        })
                        .modal('setting', 'duration', 250)
                        .modal('show')
                    ;
                });

            }
        };
    })
    .directive('daPopup', function () {
        return {
            restrict: 'A',
            link: function ($scope, $element, $attrs) {
                console.log($attrs.popup);
                $element.popup({
                    popup: $($attrs.popup),
                    on: 'hover'
                });

            }
        };
    })
    .directive('daDropdown', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                console.log('dropdown');
                element.dropdown();
            }
        };
    })
    .directive('daAccordion', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.accordion();
            }
        };
    })
    .directive('daSidebar', function () {
        return {
            restrict: 'A',
            link: function ($scope, $element, $attrs) {

                $element.bind('click', function () {
                    angular.element($attrs.daSidebar)
                        .sidebar('setting', 'transition', 'push')
                        .sidebar('setting', 'closable', true)
                        .sidebar('setting', 'dimPage', false)
                        .sidebar('setting', 'duration', 250)
                        .sidebar('toggle')
                    ;
                });

            }
        };
    })
    .directive('ngInitial', function ($parse) {
        return {
            restrict: "A",
            compile: function ($element, $attrs) {
                var initialValue = $attrs.value || $element.val();
                return {
                    pre: function ($scope, $element, $attrs) {
                        $parse($attrs.ngModel).assign($scope, initialValue);
                    }
                }
            }
        }
    });
