'use strict';


angular.module('daTabor', [])
    .directive('daTabs', function ($compile) {
        return {
            restrict: 'E',
            transclude: true,
            scope: {},
            controller: function ($scope, $element, $attrs, Global) {
                $scope.global = Global;
                var tabs = $scope.tabs = [];

                $scope.select = function (menu_item) {
                    angular.forEach(tabs, function (tab) {
                        tab.selected = false;
                    });
                    tab.selected = true;
                    $scope.current_tab = tab;
                    $scope.global.current_tab = tab;
                }


                this.addTab = function (tab) {
                    if (tab.length == 0) $scope.select(tab);
                    tabs.push(tab);
                }
            },
            template: '<div class="tabs"><div class="tab" ng-transclude></div></div>',
            replace: true
        };
    })
    .directive('daTab', function () {
        return {
            require: '^tabs',
            restrict: 'E',
            transclude: true,
            scope: {
                title: '@'
            },
            link: function ($scope, $element, $attrs, da_tabsCtrl) {
                da_tabsCtrl.addTab($scope);
            },
//            template: '<li class="menu_item"  ng-class="{active:selected}"><a ng-transclude></a></li>',
            template: '<div class="tab" ng-transclude></div>',
            replace: true
        };
    });