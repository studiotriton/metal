'use strict';


angular.module('landingDirectives', [])
    .directive('pagepartTitle', function () {
        return {
            restrict: 'EA',
            scope: {
                //content: '='
            },
            link: function ($scope, $element, $attrs) {
                try {
                    $scope.content = $scope.$parent.pagepart.title;
                    $scope.show_title = $scope.$parent.pagepart.show_title;
                } catch (err) {
                }
            },
            templateUrl: '/static/templates/title.html',
            replace: true
        };
    })
    .directive('pagepartTeaser', function () {
        return {
            restrict: 'EA',
            //transclude: true,
            scope: {},
            link: function ($scope, $element, $attrs) {
                try {
                    $scope.content = $scope.$parent.pagepart.teaser;
                } catch (err) {

                }
            },
            templateUrl: '/static/templates/teaser.html',
            replace: true
        };
    })
    .directive('pagepartBody', function () {
        return {
            restrict: 'EA',
            scope: {},
            link: function ($scope, $element, $attrs, $compile) {

                    var body = $scope.$parent.pagepart.body;
                    $compile(body)($scope);
                    console.log($scope);
                    //$scope.content = $scope.$parent.pagepart.body;

                try {
                } catch (err) {
                    console.log(err);

                }
            },
            templateUrl: '/static/templates/body.html',
            replace: true
        };
    })
    .directive('pagepartFeaser', function () {
        return {
            restrict: 'EA',
            //transclude: true,
            scope: {
            },
            link: function ($scope, $element, $attrs) {
                try {
                    $scope.content = $scope.$parent.pagepart.feaser;
                } catch (err) {

                }
            },
            templateUrl: '/static/templates/feaser.html',
            replace: true
        };
    });
